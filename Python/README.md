Due progetti fatti per l'esame di Machine learning

Nel primo progetto si dovevano replicare i risultati di un articolo sugli agenti inquinanti in Cile.
Nel secondo la richiesta era:

"You are invited to analyze a set of document, characterized by title and abstract on Medical Imaging for COVID-19, label them with a set of well-defined key terms and apply sentiment analysis to determine how they can be grouped together.

* define research questions
* define document-based corpus
* use NLP to preprocess data
* apply sentiment analysis and topic modeling"